using Core.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using camtt;
using camtt.Services;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initialization Main");
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler((e, r) =>
            {
                Console.WriteLine();
                Console.WriteLine(r.ExceptionObject);
                Thread.Sleep(2000);
            });

            var rootPath = System.IO.Path.GetPathRoot(AppContext.BaseDirectory);
            var logDir = System.IO.Path.Combine(rootPath, "logs", "TGToSpeech");
            System.IO.Directory.CreateDirectory(logDir);
            var logPath = System.IO.Path.Combine(logDir, "app.log");

            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("cfg.json")
                .Build();

            var serv = new ServiceCollection()
                .AddSingleton(configuration)

                .AddLogging(loggingBuilder => loggingBuilder
                    .AddConsole())
                .AddSingleton<Config>()
                .AddSingleton<TelegramBotProvider>()
                .AddSingleton<TgLog>()
                .AddSingleton<TgBotOnMsg>()
                .AddSingleton<VandParser>()
                .BuildServiceProvider();

            {
                var log = serv.GetService<TgLog>();
                log.l("app started");
            }

            var tgVhatIds_toWhom = new List<long>();
            serv.GetService<TgBotOnMsg>().onMsg += (TgBotOnMsg.FromMsg msg) =>
              {
                  tgVhatIds_toWhom.Add(msg.FromChatId);
                  tgVhatIds_toWhom = tgVhatIds_toWhom.Distinct().ToList();
              };

            serv.GetService<VandParser>().OnFoundNewPost += async (VandParser.NewPost obj) =>
            {
                var bot = serv.GetService<TelegramBotProvider>().bot;
                var log = serv.GetService<TgLog>();

                if (!tgVhatIds_toWhom.Any())
                    log.l("There is no tgVhatIds_toWhom");

                foreach (var toWhom in tgVhatIds_toWhom)
                    await bot.SendTextMessageAsync(toWhom, obj.Title + " \r\n" + obj.Url);
            };

#if DEBUG
            Thread.Sleep(TimeSpan.FromSeconds(10));
#endif

            Task.Run(async () =>
            {
                while (true)
                {
                    var vandParser = serv.GetService<VandParser>();
                    await vandParser.Find("Казан");
                    await Task.Delay(TimeSpan.FromMinutes(1));
                }
            });

            Thread.Sleep(Timeout.Infinite);
        }
    }
}
