﻿using camtt.Services;
using System;
using System.Threading;
using Telegram.Bot;
using Telegram.Bot.Types.Enums;

namespace Core
{
    public class TgBotOnMsg
    {
        public event Action<FromMsg> onMsg;

        TelegramBotClient _bot;
        TgLog _tgLog;

        public TgBotOnMsg(TelegramBotProvider telegramBot, TgLog tgLog)
        {
            _bot = telegramBot.bot;
            _tgLog = tgLog;
            _bot.OnMessage += Bot_OnMessage;
            _bot.StartReceiving();
        }

        private void Bot_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs messageEventArgs)
        {
            var tId = string.Format("{0:d3}_newMsg ", Thread.CurrentThread.ManagedThreadId);
            _tgLog.l(tId + DateTime.Now.ToString());

            var message = messageEventArgs.Message;
            if (message == null) return;

            _tgLog.l(tId + "message type " + message.Type);
            _tgLog.l(tId + "Chat.Id from " + message.Chat.Id);
            if (message.Type == MessageType.TextMessage)
                onMsg?.Invoke(new FromMsg() { FromChatId = message.Chat.Id, Msg = message.Text });
        }

        public struct FromMsg
        {
            public string Msg { get; set; }
            public long FromChatId { get; set; }
        }
    }
}