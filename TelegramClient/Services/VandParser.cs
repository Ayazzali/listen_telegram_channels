﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using camtt.Services;
using Core.Services;
using HtmlAgilityPack;
using Newtonsoft.Json;
using OpenTl.ClientApi;
using OpenTl.ClientApi.MtProto.Exceptions;
using OpenTl.Schema;
using OpenTl.Schema.Channels;
using OpenTl.Schema.Messages;
using OpenTl.Schema.Updates;
using Telegram.Bot;
using TelegramClient.Services.Utils;
using TelegramClient.Utils;

namespace Core
{
    public class VandParser
    {
        static HttpClient _client = new HttpClient();
        string _lastTitle = string.Empty;

        Config _config;
        TgLog _logger;

        public VandParser(Config config, TgLog tgLog)
        {
            _config = config;
            _logger = tgLog;
        }

        public async Task Find(string citiName)
        {
            _logger.l("Find...");
            var response = await _client.GetAsync("https://vandrouki.ru/?s=" + citiName);
            var pageContents = await response.Content.ReadAsStringAsync();
            HtmlDocument pageDocument = new HtmlDocument();
            pageDocument.LoadHtml(pageContents);
            var post = pageDocument.DocumentNode.SelectSingleNode("//div[contains(@class,'post')][1]");
            //foreach (var post in posts)
            var url = post.SelectSingleNode("//a[contains(@class,'more-link')]").GetAttributeValue("href", "");
            var title = post.SelectSingleNode("//h2[contains(@class,'entry-title')]").InnerText;
            if (title == _lastTitle)
                return;

            _logger.l("Find found: " + title);
            OnFoundNewPost?.Invoke(new NewPost() { Title = title, Url = url });

            _lastTitle = title;
            _logger.l("Find DONE");
        }

        public event Action<NewPost> OnFoundNewPost;

        public struct NewPost
        {
            public string Title { get; set; }
            public string Url { get; set; }
        }
    }
}
